import { UIController } from "./UIController";
import { UILayer, UIMgr } from "./UIMgr";
import { UI_MyInfo } from "../resources/ui/MyInfo/UI_MyInfo";

export class HUD extends UIController {

    constructor() {
        super('ui/HUD', UILayer.HUD);
    }

    protected onCreated() {

        //箭头函数直接监听事件，无法移除事件
        this.onButtonEvent('btn_info', () => {
            //弹出信息窗口
            UIMgr.inst.showUI(UI_MyInfo);
        });
    }
}